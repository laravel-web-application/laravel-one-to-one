# Laravel One To One Relationship
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-one-to-one.git`
2. Go inside the folder: `cd laravel-one-to-one`
3. Create your database and change in `.env` file
4. Run SQL Script: `Laravel_OneToOne.sql` file
5. Run `php artisan serve`
6. Open your favorite browser: http://localhost:8000/pengguna

### Screen shot

List Pengguna

![List Pengguna](img/pengguna.png "List Pengguna")

Table Relationship

![Table Relationship](img/relationship1.png "Table Relationship")

![Table Relationship](img/relationship2.png "Table Relationship")

![Table Relationship](img/relationship3.png "Table Relationship")

